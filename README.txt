P.O.R.S Alkalmazás


Tartalom
------------------------------------------------------------------------------------------------------------------------

Mappák:

  db - Tartalmazza az init.sql fájlt ami betölti az adatbázis sémákat és alapértelmezett részleget. A telepítőnél
  használt.

  src - A forráskód és egység tesztek.

  target - A mappa tartalmazza a lefordított osztályokat és az applikáció végleges jar fájlt.

  .env - Környezetváltozó értekeket tartalmaz. Elsősorban a fejlesztés során használt az alkalmazás teszteléshez
  és futtatáshoz. A pors.env fájl az ún. production környezetváltozókat tartalmazza amit a telepítés és éles
  környezetben beolvasásra kerúlnek.

  docker-compose.yml - A docker compose fájl ami leírja az adatbázis szerver paramétereit. Az .env
  fájl értékeiből táplálkozva futtatja az előbb említett adatbázist. Csakis az alkalmazás teszteléséhez
  használt. Egy külön compose fájl kerűl az éles környezetbe a telepítő által ami mind az alkalmazás, mind
  az adatbázís szolgáltatásokat tartalmazza.

  package-jar.sh - Shell szkript ami lefordítja az összes kódot egy jar fájlba ami az éles környezetbe kerűl
  telepítésre.

  pom.xml - Az alkalmazás függőségei és jar lefordításához szükséges utasításokat tartalmaz.

  pors.env - Az éles környezetváltozó sablon ami bemásolódik a telepítő csomagba.

  porstgresql.conf - Az adatbázis konfigurációs fájlja ami bemásolódik a telepítő csomagba.