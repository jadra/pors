package xyz.k1fli.pors.mapper;

import org.apache.ibatis.annotations.Mapper;
import xyz.k1fli.pors.domain.Pager;
import xyz.k1fli.pors.domain.Supplier;

import java.util.List;

/**
 * A SupplierMapper áthidalja a SupplierMapper.xml-ben élő
 * lekérdezéseket a java világába.
 */

@Mapper
public interface SupplierMapper {
    void insert(Supplier supplier);
    Pager getPagingInfo(String name, int rowsPerPage);
    List<Supplier> getAll(String name, Pager pager);
    List<Supplier> getForList();
    Supplier getByName(String name);
    Supplier getById(long id);
    void update(Supplier supplier);
    void delete(Supplier supplier);
}
