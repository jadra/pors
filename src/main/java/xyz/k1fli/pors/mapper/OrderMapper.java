package xyz.k1fli.pors.mapper;

import org.apache.ibatis.annotations.Mapper;
import xyz.k1fli.pors.controller.form.OrderFilterForm;
import xyz.k1fli.pors.domain.Order;
import xyz.k1fli.pors.domain.OrderItem;
import xyz.k1fli.pors.domain.Pager;

import java.util.List;

/**
 * Az OrderMapper áthidalja a OrderMapper.xml-ben élő
 * lekérdezéseket a java világába.
 */

@Mapper
public interface OrderMapper {
    Pager getPagingInfo(Integer userId, OrderFilterForm orderFilterForm, int rowsPerPage);
    Order getOrder(Integer id);
    List<Order> getOrders(Integer userId, OrderFilterForm orderFilterForm, Pager pager);
    Order getOrderSummary(Integer orderId, Integer userId);
    OrderItem getOrderItem(Integer orderId, Integer lineNumber);
    void updateOrderStatus(Order order);
    void update(Order order);
    void insert(Order order);
    void addOrderItem(OrderItem item);
    void editOrderItem(OrderItem item);
    void deleteOrderItem(OrderItem item);
}
