package xyz.k1fli.pors.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.transaction.annotation.Transactional;
import xyz.k1fli.pors.domain.Pager;
import xyz.k1fli.pors.domain.User;

import java.util.List;

/**
 * A UserMapper áthidalja a UserMapper.xml-ben élő
 * lekérdezéseket a java világába.
 */

@Mapper
public interface UserMapper {
    int getAdminCount();
    Pager getPagingInfo(String username, int rowsPerPage);
    List<User> getAll(String username, Pager pager);
    User getUser(Integer id);
    User getByName(String username);
    User getById(int id);
    void update(User user);
    @Transactional
    void insertAdmin(User user);
    void changePassword(String username, String password);
    void changeUsername(String oldUsername, User user);
}
