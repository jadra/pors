package xyz.k1fli.pors.mapper;

import org.apache.ibatis.annotations.Mapper;
import xyz.k1fli.pors.domain.Pager;
import xyz.k1fli.pors.domain.Profile;


import java.util.List;

/**
 * A ProfileMapper áthidalja a ProfileMapper.xml-ben élő
 * lekérdezéseket a java világába.
 */

@Mapper
public interface ProfileMapper {
    Pager getPagingInfo(String lastname, long departmentId, int rowsPerPage);
    List<Profile> getAll(String lastname, long departmentId, Pager pager);
    Profile getById(Integer id);
    int getProfileId(Integer userId);
    void update(Profile profile);
    void insert(Profile profile);
    void delete(Integer userId);
}
