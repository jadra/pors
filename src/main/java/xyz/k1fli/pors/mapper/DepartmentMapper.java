package xyz.k1fli.pors.mapper;

import org.apache.ibatis.annotations.Mapper;
import xyz.k1fli.pors.domain.Department;
import java.util.List;

/**
 * A DepartmentMapper áthidalja a DepartmentMapper.xml-ben élő
 * lekérdezéseket a java világába.
 */
@Mapper
public interface DepartmentMapper {
    List<Department> getAll();
    void insert(Department department);
    Department getByName(String name);
    Department getById(long id);
    void update(Department department);
    void delete(Department department);
}
