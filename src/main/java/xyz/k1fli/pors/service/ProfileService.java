package xyz.k1fli.pors.service;

import lombok.RequiredArgsConstructor;
import org.apache.ibatis.exceptions.PersistenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.k1fli.pors.domain.PagedResult;
import xyz.k1fli.pors.domain.Pager;
import xyz.k1fli.pors.domain.Profile;
import xyz.k1fli.pors.mapper.ProfileMapper;


/**
 * A ProfileService elérhetővé teszi a
 * profile mapper lekérdezéseket a controllerekhez.
 */
@Service
@RequiredArgsConstructor
public class ProfileService {
    public enum Result {
        OK,
        DUPLICATE_NAME,
        ERROR
    }

    private final ProfileMapper profileMapper;

    public PagedResult<Profile> getAll(String username, long departmentId, Pager pager) {
        var thePager = profileMapper.getPagingInfo(username, departmentId, pager.getRowsPerPage());
        thePager.setCurrentPage(pager.getCurrentPage());
        var users = profileMapper.getAll(username, departmentId, thePager);
        return new PagedResult<>(users, thePager);
    }

    public Profile getById(Integer id) {
        return profileMapper.getById(id);
    }

    public Integer getProfileId(Integer userId) { return profileMapper.getProfileId(userId); }

    @Transactional
    public Result update(Profile profile) {
        try {
            profileMapper.update(profile);
        } catch (PersistenceException ex) {
            return Result.ERROR;
        }
        return Result.OK;
    }

    @Transactional
    public Result insert(Profile profile) {
        var foundProfile = profileMapper.getById(profile.getId());
        if(foundProfile == null) {
            profileMapper.insert(profile);
        } else {
            return Result.DUPLICATE_NAME;
        }
        return Result.OK;
    }

}
