package xyz.k1fli.pors.service;

import lombok.Setter;
import org.apache.ibatis.exceptions.PersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.k1fli.pors.domain.PagedResult;
import xyz.k1fli.pors.domain.Pager;
import xyz.k1fli.pors.domain.User;
import xyz.k1fli.pors.mapper.ProfileMapper;
import xyz.k1fli.pors.mapper.UserMapper;


/**
 * A UserService elérhetővé teszi a
 * user mapper lekérdezéseket a controllerekhez.
 * Ráadásul a Spring security jelszó cseréket is kezeli.
 */
@Service
public class UserService {
    public enum Result {
        OK,
        DUPLICATE_NAME,
        PASSWORD_CHANGE_ERROR,
        FOREIGN_KEY_VIOLATION
    }

    @Autowired
    @Setter
    private  JdbcUserDetailsManager jdbcUserDetailsManager;
    @Autowired
    @Setter
    private  UserMapper userMapper;
    @Autowired
    @Setter
    private  ProfileMapper profileMapper;
    @Autowired
    @Setter
    private  PasswordEncoder passwordEncoder;

    public PagedResult<User> getAll(String username, Pager pager) {
        var thePager = userMapper.getPagingInfo(username, pager.getRowsPerPage());
        thePager.setCurrentPage(pager.getCurrentPage());
        var users = userMapper.getAll(username, thePager);
        return new PagedResult<>(users, thePager);
    }

    public User getUser(Integer id) {
        return userMapper.getUser(id);
    }

    public User getByName(String username) {
        return userMapper.getByName(username);
    }

    public User getById(Integer id) {
        return userMapper.getById(id);
    }

    @Transactional
    public Result createUser(User user) {
        var userExists = jdbcUserDetailsManager.userExists(user.getUsername());
        if(!userExists) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            jdbcUserDetailsManager.createUser(user);
            var theUser = userMapper.getByName(user.getUsername());
            user.getProfile().setUserId(theUser.getId());
            profileMapper.insert(user.getProfile());
        } else {
            return Result.DUPLICATE_NAME;
        }
        return Result.OK;
    }

    @Transactional
    public Result updateUser(User user, Authentication authentication) {
        try {
            var existingUser = userMapper.getById(user.getId());

            if (existingUser != null) {
                if (user.getUsername().equals(existingUser.getUsername())) {
                    user.setPassword(existingUser.getPassword());
                    jdbcUserDetailsManager.updateUser(user);
                } else {
                    if(jdbcUserDetailsManager.userExists(user.getUsername())) {
                        return Result.DUPLICATE_NAME;
                    } else {
                        userMapper.changeUsername(existingUser.getUsername(), user);
                        if (existingUser.getUsername().equals(authentication.getName())) {
                            var userDetails = (User)authentication.getPrincipal();
                            userDetails.setUsername(user.getUsername());
                        }
                    }
                }
                profileMapper.update(user.getProfile());
            }

        } catch (DuplicateKeyException ex) {
            return Result.DUPLICATE_NAME;
        }
        return Result.OK;
    }

    @Transactional
    public Result deleteUser(User user) {
        try {
            profileMapper.delete(user.getId());
            jdbcUserDetailsManager.deleteUser(user.getUsername());
        } catch (DataIntegrityViolationException exception) {
            return Result.FOREIGN_KEY_VIOLATION;
        }
        return Result.OK;
    }

    @Transactional
    public Result changePassword(String username, String password) {
        try {
            var encryptedPassword = passwordEncoder.encode(password);
            userMapper.changePassword(username, encryptedPassword);
        } catch (PersistenceException exception) {
            return Result.PASSWORD_CHANGE_ERROR;
        }
        return Result.OK;
    }
}
