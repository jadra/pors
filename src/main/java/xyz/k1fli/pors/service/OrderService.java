package xyz.k1fli.pors.service;


import lombok.RequiredArgsConstructor;
import org.apache.ibatis.exceptions.PersistenceException;
import org.springframework.stereotype.Service;
import xyz.k1fli.pors.controller.form.OrderFilterForm;
import xyz.k1fli.pors.domain.Order;
import xyz.k1fli.pors.domain.OrderItem;
import xyz.k1fli.pors.domain.PagedResult;
import xyz.k1fli.pors.domain.Pager;
import xyz.k1fli.pors.mapper.OrderMapper;


/**
 * A OrderService elérhetővé teszi az
 * order mapper lekérdezéseket a controllerekhez.
 */
@Service
@RequiredArgsConstructor
public class OrderService {
    public enum Result {
        OK,
        ERROR
    }

    private final OrderMapper orderMapper;

    public Order getOrder(Integer id) {
        return orderMapper.getOrder(id);
    }

    public PagedResult<Order> getOrders(Integer userId, OrderFilterForm orderFilterForm, Pager pager) {
        var thePager = orderMapper.getPagingInfo(userId, orderFilterForm, pager.getRowsPerPage());
        thePager.setCurrentPage(pager.getCurrentPage());
        var orders = orderMapper.getOrders(userId, orderFilterForm, thePager);
        return new PagedResult<>(orders, thePager);
    }

    public Order getOrderSummary(Integer orderId, Integer userId) {
        return orderMapper.getOrderSummary(orderId, userId);
    }

    public OrderItem getOrderItem(Integer orderId, Integer lineNumber) {
        return orderMapper.getOrderItem(orderId, lineNumber);
    }

    public Result updateOrderStatus(Order order) {
        try {
            orderMapper.updateOrderStatus(order);
        } catch (PersistenceException ex) {
            return Result.ERROR;
        }
        return Result.OK;
    }

    public Result update(Order order) {
        try {
            orderMapper.update(order);
        } catch (PersistenceException ex) {
            return Result.ERROR;
        }
        return Result.OK;
    }

    public Result insert(Order order) {
        try {
            orderMapper.insert(order);
        } catch (PersistenceException ex) {
            return Result.ERROR;
        }
         return Result.OK;
    }

    public Result addItem(OrderItem item) {
        try {
            orderMapper.addOrderItem(item);
        } catch (PersistenceException ex) {
            return Result.ERROR;
        }
        return Result.OK;
    }

    public Result editItem(OrderItem item) {
        try {
            orderMapper.editOrderItem(item);
        } catch (PersistenceException ex) {
            return Result.ERROR;
        }
        return Result.OK;
    }

    public Result deleteItem(OrderItem item) {
        try {
            orderMapper.deleteOrderItem(item);
        } catch (PersistenceException ex) {
            return Result.ERROR;
        }
        return Result.OK;
    }
}
