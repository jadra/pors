package xyz.k1fli.pors.service;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.k1fli.pors.domain.PagedResult;
import xyz.k1fli.pors.domain.Pager;
import xyz.k1fli.pors.domain.Supplier;
import xyz.k1fli.pors.mapper.SupplierMapper;

import java.util.List;


/**
 * A SupplierService elérhetővé teszi a
 * supplier mapper lekérdezéseket a controllerekhez.
 */

@Service
@RequiredArgsConstructor
public class SupplierService {
    public enum Result {
        OK,
        DUPLICATE_NAME,
        FOREIGN_KEY_VIOLATION
    }

    private final SupplierMapper supplierMapper;

    @Transactional
    public Result createSupplier(Supplier supplier) {
        var foundSupplier = supplierMapper.getByName(supplier.getName());
        if (foundSupplier == null) {
            supplierMapper.insert(supplier);
        } else {
            return Result.DUPLICATE_NAME;
        }
        return Result.OK;
    }

    public PagedResult<Supplier> getAll(String name, Pager pager) {
        var thePager = supplierMapper.getPagingInfo(name, pager.getRowsPerPage());
        thePager.setCurrentPage(pager.getCurrentPage());
        var suppliers = supplierMapper.getAll(name, thePager);
        return new PagedResult<>(suppliers, thePager);
    }

    public List<Supplier> getAll() {
        return supplierMapper.getForList();
    }

    public Supplier getSupplierById(long id) {
        return supplierMapper.getById(id);
    }

    @Transactional
    public Result updateSupplier(Supplier supplier) {
        try {
            supplierMapper.update(supplier);
        } catch (DuplicateKeyException ex) {
            return Result.DUPLICATE_NAME;
        }
        return Result.OK;
    }

    @Transactional
    public SupplierService.Result deleteSupplier(Supplier supplier) {
        try {
            supplierMapper.delete(supplier);
        } catch (DataIntegrityViolationException exception) {
            return Result.FOREIGN_KEY_VIOLATION;
        }
        return Result.OK;
    }

}
