package xyz.k1fli.pors.service;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.k1fli.pors.domain.Department;
import xyz.k1fli.pors.mapper.DepartmentMapper;

import java.util.List;

/**
 * A DepartmentService elérhetővé teszi a
 * department mapper lekérdezéseket a controllerekhez.
 */
@Service
@RequiredArgsConstructor
public class DepartmentService {

    public enum Result {
        OK,
        DUPLICATE_NAME,
        FOREIGN_KEY_VIOLATION
    }

    private final DepartmentMapper departmentMapper;

    @Transactional
    public Result createDepartment(Department department) {
        var foundDepartment = departmentMapper.getByName(department.getName());
        if (foundDepartment == null) {
            departmentMapper.insert(department);
        } else {
           return Result.DUPLICATE_NAME;
        }
        return Result.OK;
    }

    @Transactional
    public Result updateDepartment(Department department) {
        try {
            departmentMapper.update(department);
        } catch (DuplicateKeyException ex) {
            return Result.DUPLICATE_NAME;
        }
        return Result.OK;
    }

    @Transactional
    public Result deleteDepartment(Department department) {
        try {
            departmentMapper.delete(department);
        } catch (DataIntegrityViolationException exception) {
            return Result.FOREIGN_KEY_VIOLATION;
        }
        return Result.OK;
    }

    public Department getDepartmentById(long id) {
        return departmentMapper.getById(id);
    }

    public List<Department> getDepartments() {
        return departmentMapper.getAll();
    }
}
