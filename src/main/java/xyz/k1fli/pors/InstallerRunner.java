package xyz.k1fli.pors;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import xyz.k1fli.pors.domain.Profile;
import xyz.k1fli.pors.domain.Roles;
import xyz.k1fli.pors.domain.User;
import xyz.k1fli.pors.mapper.UserMapper;
import xyz.k1fli.pors.service.ProfileService;


/**
 * Az InstallerRunner osztály biztosít egy alapértelmezett
 * rendszergazdát az alkalmazás telepítéskor.
 */
@RequiredArgsConstructor
@Component
public class InstallerRunner implements CommandLineRunner {

    private final UserMapper userMapper;
    private final ProfileService profileService;

    @Value("${pors.admin.username}")
    private String username;

    @Value("${pors.admin.password}")
    private String password;


    @Override
    public void run(String... args) {

        if (userMapper.getAdminCount() == 0) {
            var user = new User();
            user.setUsername(username);
            user.setPassword(new BCryptPasswordEncoder().encode(password));
            user.setRole(Roles.ROLE_ADMIN.toString());
            userMapper.insertAdmin(user);
            var profile = new Profile();
            profile.setDepartmentId(1);
            profile.setUserId(userMapper.getByName(username).getId());
            profileService.insert(profile);
        }
    }
}
