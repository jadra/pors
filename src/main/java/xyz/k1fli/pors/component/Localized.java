package xyz.k1fli.pors.component;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public class Localized {
    /**
     * Egy lokalizációs segítő.
     * @param messageSource - A Spring üzenet forrős.
     * @param message - Az üzenet kulcsa ami a messages_hu.properties fájlban él.
     * @return - Az adott kulcsnak a szöveges fordítása.
     */
    public static String message(MessageSource messageSource, String message) {
        return messageSource.getMessage(message, null, LocaleContextHolder.getLocale());
    }
}
