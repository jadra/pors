package xyz.k1fli.pors.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import xyz.k1fli.pors.component.Messages;

import java.util.Locale;

/**
 * Spring lokalizációs konfiguráció
 */
@Configuration
public class LocalizationConfig {

    @Value("${env.LANGUAGE}")
    private String language;

    @Value("${env.COUNTRY")
    private String country;

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
        //sessionLocaleResolver.setDefaultLocale(new Locale("Hungary","hu_HU"));
        sessionLocaleResolver.setDefaultLocale(new Locale(language,country));
        return sessionLocaleResolver;
    }

    @Bean
    public LocalValidatorFactoryBean validator(MessageSource messageSource) {
        var bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource);
        return bean;
    }
}
