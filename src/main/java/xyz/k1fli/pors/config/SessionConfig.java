package xyz.k1fli.pors.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import xyz.k1fli.pors.controller.UserInfo;

/**
 * Spring session konfiguráció.
 */
@Configuration
public class SessionConfig {

    /**
     * A beléptetett felhasználónak kulcs adatai.
     * @return A UserInfo objektum.
     */
    @Bean
    @Scope
    public UserInfo userInfo() {
        return new UserInfo();
    }
}
