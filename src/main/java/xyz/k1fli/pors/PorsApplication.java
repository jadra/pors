package xyz.k1fli.pors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PorsApplication {

    public static void main(String[] args) {
        SpringApplication.run(PorsApplication.class, args);
    }

}
