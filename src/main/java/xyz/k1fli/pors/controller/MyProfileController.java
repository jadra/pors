package xyz.k1fli.pors.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import xyz.k1fli.pors.component.Localized;
import xyz.k1fli.pors.component.Messages;
import xyz.k1fli.pors.domain.Profile;
import xyz.k1fli.pors.domain.Roles;
import xyz.k1fli.pors.domain.User;
import xyz.k1fli.pors.service.ProfileService;
import xyz.k1fli.pors.service.UserService;

import javax.annotation.Resource;


/**
 * A MyProfileController kezeli az adott felhasználóhoz
 * kötődő profillal kapcsolatos eljárásokat.
 */
@RequestMapping("/my/profile")
@Controller
@RequiredArgsConstructor
public class MyProfileController {
    @Resource(name = "userInfo")
    private UserInfo userInfo;
    private final MessageSource messageSource;
    private final ProfileService profileService;
    private final UserService userService;

    @ModelAttribute
    public User getUser() {
        return new User();
    }

    /**
     * A profilom főoldal.
     * @param model model Spring áltat nyújtott objektum.
     * @return
     *  Ha rendszergazda, /my/profile/changePassword thymeleaf sablon átírányítás.
     *  Ha normál felhasználó a my/profile/index thymeleaf sablon átírányítás.
     */
    @GetMapping
    public String index(Model model) {
        model.addAttribute("action", "/my/profile/edit");
        model.addAttribute("area", "profileEdit");

        var user = userService.getById(userInfo.getUserId());

        if (user.getRole().equals(Roles.ROLE_ADMIN.toString())) {
            return "redirect:/my/profile/changePassword";
        }

        user.setProfile(profileService.getById(userInfo.getId()));
        model.addAttribute(user);

        return "myprofile/index";
    }

    /**
     * A felhasználó profil módosítás GET megjelenítés.
     * @param user A felhasználó objektum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring áltat nyújtott objektum.
     * @param redirectAttributes Átírányítás objektum.
     * @return  Siker esetén főoldalra átirányítás.
     */
    @PostMapping("/edit")
    public String edit(@Validated({User.Update.class, Profile.PartialUpdate.class}) User user, BindingResult result,
                       Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "myprofile/index";
        }

        var serviceResult = profileService.update(user.getProfile());
        if (serviceResult != ProfileService.Result.OK) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "PROFILE_UPDATE_ERROR")));
            return "myprofile/index";
        }

        redirectAttributes.addFlashAttribute(
                new Messages().success(Localized.message(messageSource,"PROFILE_UPDATE_SUCCESS")));

        return "redirect:/my/profile";
    }

    /**
     * A felhasználó jelszó módosítás GET megjelenítés.
     * @param user A felhasználó objektum.
     * @param model Spring áltat nyújtott objektum.
     * @return myprofile/change_password thymeleaf sablon.
     */
    @GetMapping("/changePassword")
    public String changePassword(User user, Model model) {
        model.addAttribute("action", "/my/profile/changePassword");
        model.addAttribute("area", "profileChangePassword");
        return "myprofile/change_password";
    }

    /**
     * A felhasználó jelszó módosítás POST metódus.
     * @param user A felhasználó objektum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring áltat nyújtott objektum.
     * @return Siker esetén főoldalra átirányítás.
     */
    @PostMapping("/changePassword")
    public String changePassword(@Validated ({User.ChangePassword.class}) User user,
                                 BindingResult result, Model model) {

        if (result.hasErrors()) {
            return "myprofile/change_password";
        }

        var serviceResult =  userService.changePassword(userInfo.getUsername(), user.getPassword());
        if (serviceResult != UserService.Result.OK) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource,"PROFILE_PASSWORD_CHANGE_ERROR")));
            return "myprofile/change_password";
        }

        return "redirect:/auth/logout";
    }

}
