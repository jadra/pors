package xyz.k1fli.pors.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import xyz.k1fli.pors.component.Localized;
import xyz.k1fli.pors.component.Messages;
import xyz.k1fli.pors.domain.Department;
import xyz.k1fli.pors.service.DepartmentService;


/**
 * A DepartmentController kezeli a részlegekhez
 * kötődő eljárásokat.
 */
@RequestMapping("/admin/departments")
@Controller
@RequiredArgsConstructor
public class DepartmentController {
    private final DepartmentService departmentService;
    private final MessageSource messageSource;

    @ModelAttribute
    public Department getForm() {
        return new Department();
    }

    /**
     * A főooldal kezelője.
     * @param model Spring által nyújtott objektum.
     * @return department/index thymeleaf sablon.
     */
    @GetMapping
    public String index(Model model) {
        var departmentList = departmentService.getDepartments();
        model.addAttribute(departmentList);
        return "department/index";
    }

    /**
     * Részleg létrehozás GET megjelenítés.
     * @param model Spring áltat nyújtott objektum.
     * @return department/create thymeleaf sablon.
     */
    @GetMapping(path="/create")
    public String create(Model model) {
        model.addAttribute("action", "/admin/departments/create");
        return "department/create";
    }

    /**
     * Részleg létrehozás POST metódus.
     * @param department A részleg objektum ami mentésre kerül.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring által nyújtott objektum.
     * @param redirectAttributes Visszaírányítás objektum.
     * @return Siker esetén főoldalra visszavezetés.
     */
    @PostMapping("/create")
    public String create(@Validated Department department, BindingResult result, Model model,
                         RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return create(model);
        }

        var serviceResult = departmentService.createDepartment(department);
        if (serviceResult == DepartmentService.Result.DUPLICATE_NAME) {
            model.addAttribute(new Messages()
                    .error(Localized.message(messageSource, "DEPARTMENT_DUPLICATE_ERROR")));
            return create(model);
        }

        redirectAttributes.addFlashAttribute(new Messages()
                .success(department.getName()+ " " + Localized.message(messageSource,"INSERT_SUCCESS")));
        return "redirect:/admin/departments";
    }

    /**
     * Részleg módosítás űrlap megjelenítés.
     * @param id Részleg azonosító
     * @param model Spring által nyújtott objektum.
     * @return department/edit thymeleaf sablon.
     */
    @GetMapping(path = "/edit")
    public String edit(@RequestParam long id, Model model) {
        model.addAttribute("action", "/admin/departments/edit");
        var department = departmentService.getDepartmentById(id);
        model.addAttribute(department);
        return "department/edit";
    }

    /**
     * Részleg módosítás POST metódus.
     * @param department A részleg objektum ami módosításra kerül.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring által nyújtott objektum.
     * @param redirectAttributes siker esetén főoldalra visszavezetés.
     * @return siker esetén főoldalra visszavezetés.
     */
    @PostMapping(path ="/edit")
    public String edit(@Validated Department department, BindingResult result, Model model,
                       RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "department/edit";
        }

        var serviceResult = departmentService.updateDepartment(department);
        if (serviceResult == DepartmentService.Result.DUPLICATE_NAME) {
            model.addAttribute(new Messages()
                    .error(Localized.message(messageSource,"DEPARTMENT_DUPLICATE_ERROR")));
            return edit(department.getId(), model);
        }

        redirectAttributes.addFlashAttribute(new Messages()
                .success(Localized.message(messageSource,"DEPARTMENT_UPDATE_SUCCESS")));
        return "redirect:/admin/departments";
    }

    /**
     * Részleg módosítás űrlap megjelenítés.
     * @param id részleg azonosító.
     * @param model Spring által nyújtott objektum.
     * @return department/delete thymeleaf sablon.
     */
    @GetMapping(path = "/delete")
    public String delete(@RequestParam long id, Model model) {
        model.addAttribute("action", "/admin/departments/delete");
        var department =departmentService.getDepartmentById(id);
        model.addAttribute(department);
        return "department/delete";
    }

    /**
     * Részleg törlés POST metódus.
     * @param department A részleg objektum ami törlésre kerül.
     * @param id részleg azonosító.
     * @param model Spring által nyújtott objektum.
     * @param redirectAttributes siker esetén főoldalra visszavezetés.
     * @return siker esetén főoldalra visszavezetés.
     */
    @PostMapping(path ="/delete")
    public String delete(Department department, @RequestParam long id, Model model,
                         RedirectAttributes redirectAttributes) {
        model.addAttribute("action", "/admin/departments/delete");
        var serviceResult = departmentService.deleteDepartment(department);
        if (serviceResult == DepartmentService.Result.FOREIGN_KEY_VIOLATION) {
            var message = Localized.message(messageSource, "DEPARTMENT_FOREIGN_KEY_ERROR");
            model.addAttribute(
                    new Messages().error(message));
            return delete(id, model);
        }

        redirectAttributes.addFlashAttribute(new Messages()
                .success(Localized.message(messageSource,"DEPARTMENT_DELETE_SUCCESS")));
        return "redirect:/admin/departments";
    }
}
