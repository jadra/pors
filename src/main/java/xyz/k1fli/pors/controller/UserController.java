package xyz.k1fli.pors.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import xyz.k1fli.pors.component.Localized;
import xyz.k1fli.pors.component.Messages;
import xyz.k1fli.pors.controller.form.UserFilterForm;
import xyz.k1fli.pors.domain.User;
import xyz.k1fli.pors.service.DepartmentService;
import xyz.k1fli.pors.service.UserService;

import javax.annotation.Resource;
import javax.validation.groups.Default;
import java.util.Optional;


/**
 * A UserController kezeli a felhasználókhoz
 * kötődő eljárásokat.
 */
@RequestMapping("/admin/users")
@Controller
public class UserController {
    @Resource(name = "userInfo")
    private UserInfo userInfo;
    @Autowired
    private  MessageSource messageSource;
    @Autowired
    private UserService userService;
    @Autowired
    private DepartmentService departmentService;

    @ModelAttribute
    public User setupUser() {
        return new User();
    }

    /**
     * A felhasználók főoldal.
     * @param userFilterForm A felhasználó szűrés paraméterek.
     * @param model Spring áltat nyújtott objektum.
     * @param currentPage A kért lapszám.
     * @return user/index thymeleaf sablon.
     */
    @GetMapping
    public String index(UserFilterForm userFilterForm,
                        Model model,
                        @RequestParam("currentPage") Optional<Integer> currentPage) {

        var map = userFilterForm.toMap();
        var pager = userInfo.saveUri("/admin/users", map, currentPage.orElse(0));
        var result = userService.getAll(userFilterForm.getUsername(), pager);

        model.addAttribute(result.getResults());
        model.addAttribute(result.getPager());

        return "user/index";
    }

    /**
     * A szűrés eljárás. Az adott paraméterek a userInfo session-be kerülnek
     * mentésre.
     * @param userFilterForm A Szűrés objektum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @return Siker esetén főoldal átirányítás.
     */
    @GetMapping("/filter")
    public String filter(@Validated UserFilterForm userFilterForm, BindingResult result) {
        if (result.hasErrors()) {
            return "user/index";
        }
        var map = userFilterForm.toMap();
        userInfo.saveUri("/admin/users", map, 0);
        return "redirect:"+userInfo.getUri("/admin/users");
    }

    /**
     * Szűrés törlése.
     * @return főoldalra átirányítás.
     */
    @PostMapping("/clear")
    public String clear() {
        userInfo.clearUri();
        return "redirect:/admin/users";
    }

    /**
     * Felhasználó létrehozás GET megjelenítés.
     * @param model Spring áltat nyújtott objektum.
     * @return user/create thymeleaf sablon.
     */
    @GetMapping(path = "/create")
    public String create(Model model) {
        model.addAttribute("action", "/admin/users/create");
        model.addAttribute(departmentService.getDepartments());
        return "user/create";
    }

    /**
     * Felhasználó létrehozás POST metódus.
     * @param user A felhasználó objektum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring áltat nyújtott objektum.
     * @param redirectAttributes Átírányítás objektum.
     * @return Siker esetén főoldalra átirányítás.
     */
    @PostMapping(path = "/create")
    public String create(@Validated({Default.class, User.Create.class}) User user,
                         BindingResult result,
                         Model model,
                         RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return create(model);
        }

        var serviceResult = userService.createUser(user);

        if (serviceResult == UserService.Result.DUPLICATE_NAME) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "USER_DUPLICATE_ERROR")));
            return create(model);
        }

        redirectAttributes.addFlashAttribute(
                new Messages().success(
                        user.getUsername()+ " " + Localized.message(messageSource, "INSERT_SUCCESS")));
        return "redirect:/admin/users";
    }

    /**
     * Felhasználó módosítás GET megjelenítés.
     * @param id A felhasználó azonosítója.
     * @param model Spring áltat nyújtott objektum.
     * @return /user/edit thymeleaf sablon.
     */
    @GetMapping("/edit")
    public String edit(@RequestParam Integer id, Model model) {
        model.addAttribute("action", "/admin/users/edit");
        var user = userService.getUser(id);
        model.addAttribute(departmentService.getDepartments());
        model.addAttribute(user);
        return "user/edit";
    }

    /**
     * Felhasználó módosítás POST metódus.
     * @param user A felhasználó objetkum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring áltat nyújtott objektum.
     * @param redirectAttributes Átírányítás objektum.
     * @return Siker esetén főoldalra átirányítás.
     */
    @PostMapping(path = "/edit")
    public String edit(@Validated({Default.class, User.Update.class}) User user,
                       BindingResult result,
                       Model model,
                       RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            model.addAttribute(departmentService.getDepartments());
            return "user/edit";
        }

        var authentication = SecurityContextHolder.getContext().getAuthentication();
        var serviceResult = userService.updateUser(user, authentication);
        if (serviceResult == UserService.Result.DUPLICATE_NAME) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "USER_DUPLICATE_ERROR")));
            return edit(user.getId(), model);
        }

        redirectAttributes.addFlashAttribute(
                new Messages().success(Localized.message(messageSource, "USER_UPDATE_SUCCESS")));
        return "redirect:/admin/users";
    }

    /**
     * Felhasználó törlés GET megjelenítés.
     * @param id A felhasználó azonosítója.
     * @param model Spring áltat nyújtott objektum.
     * @return user/delete thymeleaf sablon.
     */
    @GetMapping("/delete")
    public String delete(@RequestParam Integer id, Model model) {
        model.addAttribute("action", "/admin/users/delete");
        model.addAttribute(userService.getUser(id));
        return "user/delete";
    }

    /**
     * Felhasználó törlés POST metódus.
     * @param user A felhasználó objektum.
     * @param model Spring áltat nyújtott objektum.
     * @param redirectAttributes Átírányítás objektum.
     * @return Siker esetén főoldalra átirányítás.
     */
    @PostMapping("/delete")
        public String delete(User user, Model model,
                RedirectAttributes redirectAttributes) {
        model.addAttribute("action", "/admin/users/delete");
        var serviceResult = userService.deleteUser(user);
        if (serviceResult == UserService.Result.FOREIGN_KEY_VIOLATION) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "USER_DELETE_FAILURE")));
            return "user/delete";
        }
        redirectAttributes.addFlashAttribute(
                new Messages().success(Localized.message(messageSource, "USER_DELETE_SUCCESS")));
        return "redirect:/admin/users";
    }

    /**
     * Felhasználó jelszó módosítás GET megjelenítés.
     * @param id A felhasználó azonosítója.
     * @param model Spring áltat nyújtott objektum.
     * @return user/change_password thymeleaf sablon.
     */
    @GetMapping("/changepass")
    public String changePassword(@RequestParam Integer id, Model model) {
        model.addAttribute("action", "/admin/users/changepass");
        var user = userService.getUser(id);
        model.addAttribute(user);
        return "user/change_password";
    }

    /**
     * Felhasználó jelszó POST metódus.
     * @param user A felhasználó objektum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring áltat nyújtott objektum.
     * @param redirectAttributes Átírányítás objektum.
     * @return Siker esetén főoldalra átirányítás.
     */
    @PostMapping("/changepass")
    public String changePassword(@Validated({User.ChangePassword.class}) User user,
                                 BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "user/change_password";
        }

        var serviceResult = userService.changePassword(user.getUsername(), user.getPassword());
        if (serviceResult == UserService.Result.PASSWORD_CHANGE_ERROR) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "PASSWORD_CHANGE_FAILURE")));
            return "user/change_password";
        }


        redirectAttributes.addFlashAttribute(
                new Messages().success(user.getUsername() + " " +
                        Localized.message(messageSource, "PASSWORD_CHANGE_SUCCESS")));
        return "redirect:/admin/users";
    }
}
