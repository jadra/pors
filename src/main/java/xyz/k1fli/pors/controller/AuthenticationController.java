package xyz.k1fli.pors.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.k1fli.pors.component.Localized;
import xyz.k1fli.pors.component.Messages;
import xyz.k1fli.pors.domain.User;
import xyz.k1fli.pors.service.ProfileService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Az AuthenticationController kezeli a be és kiléptetést.
 * A Spring Security keretrendszer alapértelmezett
 * belépés űrlapját írja felül.
 */
@Controller
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    @Resource(name = "userInfo")
    private UserInfo userInfo;
    private final MessageSource messageSource;
    private final ProfileService profileService;

    @GetMapping("/login")
    public String login(@RequestParam Optional<Boolean> error, Model model) {
        error.ifPresent(loginError -> {
            if (loginError) {
                model.addAttribute(
                        new Messages().error(Localized.message(messageSource, "LOGIN_FAILED")));
            }
        });
        return "security/login";
    }

    @PostMapping("/loginSuccess")
    public String login(@AuthenticationPrincipal User user) {
        userInfo.setUsername(user.getUsername());
        userInfo.setId(profileService.getProfileId(user.getId()));
        userInfo.setUserId(user.getId());
        return "redirect:/my/orders";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }
        return "redirect:/login";
    }
}
