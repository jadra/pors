package xyz.k1fli.pors.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import xyz.k1fli.pors.component.Localized;
import xyz.k1fli.pors.component.Messages;
import xyz.k1fli.pors.controller.form.SupplierFilterForm;
import xyz.k1fli.pors.domain.Supplier;
import xyz.k1fli.pors.service.SupplierService;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * A SupplierController kezeli a beszállítokhoz
 * kötődő eljárásokat.
 */
@RequestMapping("/admin/suppliers")
@Controller
@RequiredArgsConstructor
public class SupplierController {
    @Resource(name = "userInfo")
    private UserInfo userInfo;
    private final SupplierService supplierService;
    private final MessageSource messageSource;

    @ModelAttribute
    public Supplier getForm() { return new Supplier(); }

    /**
     * A beszállítók főoldal.
     * @param filterForm Beszállító szúrés paraméterek.
     * @param model Spring áltat nyújtott objektum.
     * @param currentPage A kért lapszám.
     * @return supplier/index thymeleaf sablon.
     */
    @GetMapping
    public String index(SupplierFilterForm filterForm,
                        Model model,
                        @RequestParam("currentPage")  Optional<Integer> currentPage) {

        var map = filterForm.toMap();
        var pager = userInfo.saveUri("/admin/suppliers", map, currentPage.orElse(0));
        var result = supplierService
                .getAll(filterForm.getOptName().orElse(null), pager);
        model.addAttribute(result.getResults());
        model.addAttribute(result.getPager());
        return "supplier/index";
    }

    /**
     * A szűrés eljárás. Az adott paraméterek a userInfo session-be kerülnek
     * mentésre.
     * @param filterForm A Szűrés objektum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @return Siker esetén főoldal átirányítás.
     */
    @GetMapping("/filter")
    public String filter(@Validated SupplierFilterForm filterForm, BindingResult result) {
        if (result.hasErrors()) {
            return "supplier/index";
        }
        var map = filterForm.toMap();
        userInfo.saveUri("/admin/suppliers", map, 0);
        return "redirect:"+userInfo.getUri("/admin/suppliers");
    }

    /**
     * Szűrés törlése.
     * @return főoldalra átirányítás.
     */
    @PostMapping("/clear")
    public String clear() {
        userInfo.clearUri();
        return "redirect:/admin/suppliers";
    }

    /**
     * Beszállító létrehozás GET megjelenítés.
     * @param model Spring áltat nyújtott objektum.
     * @return supplier/create thymeleaf sablon.
     */
    @GetMapping(path="/create")
    public String create(Model model) {
        model.addAttribute("action", "/admin/suppliers/create");
        return "supplier/create";
    }

    /**
     * Beszállító létrehozás POST metódus.
     * @param supplier A beszállító objektum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring áltat nyújtott objektum.
     * @param redirectAttributes Átírányítás objektum.
     * @return Siker esetén főoldalra átirányítás.
     */
    @PostMapping("/create")
    public String create(@Validated Supplier supplier, BindingResult result, Model model,
                         RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return create(model);
        }

        var serviceResult = supplierService.createSupplier(supplier);
        if (serviceResult == SupplierService.Result.DUPLICATE_NAME) {
            model.addAttribute(
                    new Messages().error(
                            Localized.message(messageSource, "SUPPLIER_DUPLICATE_ERROR")));
            return create(model);
        }

        redirectAttributes.addFlashAttribute(
                new Messages().success(
                        supplier.getName()+ " " + Localized.message(messageSource, "INSERT_SUCCESS")));
        return "redirect:/admin/suppliers";
    }

    /**
     * Beszállító módosítás GET megjelenítés.
     * @param id Beszállító azonosító.
     * @param model Spring áltat nyújtott objektum.
     * @return supplier/edit thymeleaf sablon.
     */
    @GetMapping(path = "/edit")
    public String edit(@RequestParam Integer id, Model model) {
        model.addAttribute("action", "/admin/suppliers/edit");
        var supplier = supplierService.getSupplierById(id);
        model.addAttribute(supplier);
        return "supplier/edit";
    }

    /**
     * Beszállító módosítás POST metódus.
     * @param supplier A beszállító objetkum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring áltat nyújtott objektum.
     * @param redirectAttributes Átírányítás objektum.
     * @return Siker esetén főoldalra átirányítás.
     */
    @PostMapping(path ="/edit")
    public String edit(@Validated Supplier supplier, BindingResult result, Model model,
                       RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "supplier/edit";
        }

        var serviceResult = supplierService.updateSupplier(supplier);
        if (serviceResult == SupplierService.Result.DUPLICATE_NAME) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "SUPPLIER_DUPLICATE_ERROR")));
            return "supplier/edit";
        }

        redirectAttributes.addFlashAttribute(
                new Messages().success(Localized.message(messageSource,"SUPPLIER_UPDATE_SUCCESS")));
        return "redirect:/admin/suppliers";
    }

    /**
     * Beszállító törlés GET megjelenítés.
     * @param id A beszállító azonosítója.
     * @param model Spring áltat nyújtott objektum.
     * @return supplier/delete thymeleaf sablon.
     */
    @GetMapping(path = "/delete")
    public String delete(@RequestParam Integer id, Model model) {
        model.addAttribute("action", "/admin/suppliers/delete");
        var supplier = supplierService.getSupplierById(id);
        model.addAttribute(supplier);
        return "supplier/delete";
    }

    /**
     * Beszállító törlés POST metódus.
     * @param supplier A beszállító objetkum.
     * @param model Spring áltat nyújtott objektum.
     * @param redirectAttributes Átírányítás objektum.
     * @return Siker esetén főoldalra átirányítás.
     */
    @PostMapping(path ="/delete")
    public String delete(Supplier supplier, Model model,
                         RedirectAttributes redirectAttributes) {
        model.addAttribute("action", "/admin/suppliers/delete");
        var serviceResult = supplierService.deleteSupplier(supplier);
        if (serviceResult == SupplierService.Result.FOREIGN_KEY_VIOLATION) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "SUPPLIER_FOREIGN_KEY_ERROR")));
            return "supplier/delete";
        }

        redirectAttributes.addFlashAttribute(
                new Messages().success(Localized.message(messageSource, "SUPPLIER_DELETE_SUCCESS")));
        return "redirect:/admin/suppliers";
    }

}
