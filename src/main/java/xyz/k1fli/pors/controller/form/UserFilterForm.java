package xyz.k1fli.pors.controller.form;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Getter
@Setter
public class UserFilterForm {
    @NotBlank(message = "{username.not.blank}")
    private String username;

    public Map<String, Optional<?>> toMap() {
        var map = new HashMap<String, Optional<?>>();
        if (username != null) {
            map.put("username", Optional.of(username));
        }
        return map;
    }
}
