package xyz.k1fli.pors.controller.form;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Az SupplierFilterForm kezeli a beszállító szűrés paramétereket.
 * A spring keretrendszer adja meg a request-ből értékeket.
 */
@Getter
@Setter
public class SupplierFilterForm {
    @NotBlank(message = "{name.not.blank}")
    private String name;

    public Optional<String> getOptName() {
        return  Optional.ofNullable(name);
    }

    public Map<String, Optional<?>> toMap() {
        var map = new HashMap<String, Optional<?>>();
        if (name != null) {
            map.put("name", Optional.of(name));
        }
        return map;
    }
}
