package xyz.k1fli.pors.controller.form;

import lombok.Getter;
import lombok.Setter;
import xyz.k1fli.pors.domain.Order;
import xyz.k1fli.pors.domain.Supplier;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.List;

/**
 * Az OrderForm kezeli a rendelés űrlapok összegyűjtését
 * és validációját a Spring által.
 */
@Getter
@Setter
public class OrderForm implements Serializable {

    private List<Supplier> suppliers;
    @Valid
    private Order order;

    public Order toOrder() {
        return order;
    }

    public static OrderForm fromOrder(Order order) {
        var orderForm = new OrderForm();
        orderForm.setOrder(order);
        return orderForm;
    }

    public static OrderForm fromOrder(Order order, Supplier selectedSupplier) {
        order.setSupplier(selectedSupplier);
        return OrderForm.fromOrder(order);
    }

    public static OrderForm fromOrder(Order order, Supplier selectedSupplier, List<Supplier> suppliers)
    {
        var form = OrderForm.fromOrder(order, selectedSupplier);
        form.setSuppliers(suppliers);
        form.setOrder(order);
        return form;
    }
}
