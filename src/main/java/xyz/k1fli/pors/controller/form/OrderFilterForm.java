package xyz.k1fli.pors.controller.form;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Az OrderFilterForm kezeli a rendelés szűrés paramétereket.
 * A spring keretrendszer adja meg a request-ből értékeket.
 */
@Getter
@Setter
public class OrderFilterForm implements Serializable {
    private String daysAgo;
    private String orderStatus;

    /**
     * A toMap funkció létrehoz egy map-ot
     * az OrderFilterForm tulajdonságaibol.
     * @return Map az opcionális értekekkel és tulajdonság kulcsaival.
     */
    public Map<String, Optional<?>> toMap() {
        var map = new HashMap<String, Optional<?>>();
        if (daysAgo != null) {
            map.put("daysAgo", Optional.of(daysAgo));
        }
        if (orderStatus != null) {
            map.put("orderStatus", Optional.of(orderStatus));
        }
        return map;
    }
}
