package xyz.k1fli.pors.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.annotation.Resource;

/**
 * A GlobalControllerAdvice objektum elérhetővé
 * teszi a userInfo objetkumot az összes
 * thymeleaf sablonnak.
 */
@ControllerAdvice
public class GlobalControllerAdvice {
    @Resource(name = "userInfo")
    private UserInfo userInfo;


    @ModelAttribute("userInfo")
    public UserInfo populateUserInfo() {
        return userInfo;
    }
}
