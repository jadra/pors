package xyz.k1fli.pors.controller;

import lombok.*;
import org.springframework.web.util.UriComponentsBuilder;
import xyz.k1fli.pors.domain.Pager;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * A UserInfo osztály tartalmazza a felhasználó fontos
 * session-be tárolódó értékeket.
 */
public class UserInfo {
    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private Integer userId;
    @Getter
    @Setter
    private String username;

    @Getter
    private String path;

    private final Map<String, Optional<?>> parameters;

    public UserInfo() {
        parameters = new HashMap<>();
    }


    /**
     * Szűrés paraméter mentés és lapozás objektum példányosítás.
     * @param path Az elérési út ami tárolásra kerül.
     * @param parameters Az opcionális szúrés/lapozás paraméterek.
     * @param currentPage A jelenlegi oldalszám.
     * @return Pager objektum.
     */
    public  Pager saveUri(String path, Map<String, Optional<?>> parameters, Integer currentPage) {
        if (path.equals(this.path)) {
            this.parameters.put("currentPage", Optional.of(currentPage));
        } else{
            this.path = path;
            this.parameters.put("currentPage", Optional.of(0));
        }
        this.parameters.putAll(parameters);
        var pager = new Pager();
        pager.setCurrentPage(currentPage);
        pager.setRowsPerPage(10);
        return pager;
    }

    /**
     * Visszadja a jelenleg tárolt, paraméterekkel ellátott URI string-et.
     * @param path Az elérési út.
     * @return Az elérést.
     */
    public String getUri(String path) {
        if (this.path.equals(path)) {
            var builder  = UriComponentsBuilder.fromPath(path);
            parameters.forEach(builder::queryParamIfPresent);
            return builder.toUriString();
        }
        return path;
    }

    /**
     * A tárolt Uri törlése.
     */
    public void clearUri() {
        parameters.clear();
        path = null;
    }

}
