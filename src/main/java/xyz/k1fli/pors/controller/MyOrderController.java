package xyz.k1fli.pors.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import xyz.k1fli.pors.component.Localized;
import xyz.k1fli.pors.component.Messages;
import xyz.k1fli.pors.controller.form.OrderFilterForm;
import xyz.k1fli.pors.controller.form.OrderForm;
import xyz.k1fli.pors.domain.Order;
import xyz.k1fli.pors.domain.OrderItem;
import xyz.k1fli.pors.domain.OrderStatus;
import xyz.k1fli.pors.service.OrderService;
import xyz.k1fli.pors.service.SupplierService;
import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Optional;

/**
 * A MyOrderController kezeli az adott felhasználóhoz
 * kötődő rendeléssel kapcsolatos eljárásokat.
 */
@RequestMapping(value = {"/my/orders","/"})
@Controller
@RequiredArgsConstructor
public class MyOrderController {
    @Resource(name = "userInfo")
    private UserInfo userInfo;
    private final MessageSource messageSource;
    private final OrderService orderService;
    private final SupplierService supplierService;

    @ModelAttribute
    public OrderForm setupForm() {
        var form = new OrderForm();
        form.setSuppliers(supplierService.getAll());
        return form;
    }

    @ModelAttribute
    public OrderItem setupOrderItem() {
        return new OrderItem();
    }

    /**
     * A rendelés főoldala.
     * @param orderFilterForm Rendelés szúrés paraméterek.
     * @param currentPage A kért lapszám.
     * @param model Spring áltat nyújtott objektum.
     * @return myorder/index thymeleaf sablon.
     */
    @GetMapping
    public String index(OrderFilterForm orderFilterForm,
                        @RequestParam Optional<Integer> currentPage,
                        Model model) {

        var map = orderFilterForm.toMap();
        var pager = userInfo.saveUri("/my/orders", map, currentPage.orElse(0));
        var pagedResult = orderService
                .getOrders(userInfo.getId(), orderFilterForm, pager);
        model.addAttribute(pagedResult.getResults());
        model.addAttribute(pagedResult.getPager());

        return "myorder/index";
    }

    /**
     * Szűrés törlése.
     * @return főoldalra átirányítás.
     */
    @PostMapping("/clear")
    public String clear() {
        userInfo.clearUri();
        return "redirect:/my/orders";
    }

    /**
     * Rendelés megjelenítés.
     * @param id rendeles azonosító.
     * @param model Spring áltat nyújtott objektum.
     * @return myorder/show thymeleaf sablon.
     */
    @GetMapping("/show")
    public String show(@RequestParam Integer id, Model model) {
        var order = orderService.getOrder(id);
        order.setSupplier(supplierService.getSupplierById(order.getSupplierId()));
        model.addAttribute(order);
        return "myorder/show";
    }

    /**
     * Rendelés létrehozás GET megjelenítése.
     * @param orderForm A rendelés objektum.
     * @param model Spring áltat nyújtott objektum.
     * @return myorder/create thymeleaf sablon.
     */
    @GetMapping("/create")
    public String create(OrderForm orderForm, Model model) {
        model.addAttribute("action", "/my/orders/create");
        var order = new Order();
        order.setUserId(userInfo.getId());
        orderForm.setOrder(order);
        model.addAttribute(orderForm);
        return "myorder/create";
    }

    /**
     * Rendelés létrehozás POST metódus.
     * @param orderForm A rendelés objektum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring áltat nyújtott objektum.
     * @return Siker esetén főoldalra visszavezetés.
     */
    @PostMapping("/create")
    public String create(@Valid OrderForm orderForm, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return create(orderForm, model);
        }

        var order = orderForm.toOrder();
        var serviceResult = orderService.insert(order);
        if (serviceResult != OrderService.Result.OK) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "ORDER_INSERT_ERROR")));
            return create(orderForm, model);
        }

        return "redirect:/my/orders/edit?id="+order.getId();
    }

    /**
     * Rendelés szerkeztés GET megjelenítése.
     * @param id Rendelés azonosító
     * @param model Spring áltat nyújtott objektum.
     * @return myorder/edit thymeleaf sablon.
     */
    @GetMapping("/edit")
    public String edit(@RequestParam Integer id, Model model) {
        model.addAttribute("action", "/my/orders/edit");
        var order = orderService.getOrder(id);
        var orderForm = OrderForm.fromOrder(
                            order,
                            supplierService.getSupplierById(order.getSupplierId()),
                            supplierService.getAll());
        model.addAttribute(orderForm);
        return "myorder/edit";
    }

    /**
     * Rendelés szerkeztés POST metódus.
     * @param orderForm Rendelés objetkum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring áltat nyújtott objektum.
     * @return Siker esetén főoldalra visszavezetés.
     */
    @PostMapping("/edit")
    public String edit(@Validated OrderForm orderForm, BindingResult result, Model model) {
        if (result.hasErrors()) {
            setOrderItems(orderForm);
            return "myorder/edit";
        }

        var serviceResult = orderService.update(orderForm.getOrder());
        if (serviceResult != OrderService.Result.OK) {
            setOrderItems(orderForm);
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "ORDER_SUPPLIER_UPDATE_ERROR")));
            return "myorder/edit";
        }
        return "redirect:/my/orders/edit?id="+orderForm.getOrder().getId();
    }

    /**
     * Privát metódus ami betölti a rendelés tételeit.
     * @param orderForm A rendelés objektum.
     */
    private void setOrderItems(OrderForm orderForm) {
        var order = orderService.getOrder(orderForm.getOrder().getId());
        orderForm.getOrder().setItems(order.getItems());
    }

    /**
     * Rendelés lezárás GET megjelenítése.
     * @param id Rendelés azonosító.
     * @param status A kivánt státusz.
     * @param model Spring áltat nyújtott objektum.
     * @return myorder/close thymeleaf sablon.
     */
    @GetMapping("/close")
    public String closeOrder(@RequestParam Integer id, @RequestParam Integer status, Model model) {
        model.addAttribute("action", "/my/orders/close");
        var order = orderService.getOrderSummary(id, userInfo.getId());
        model.addAttribute(order);
        model.addAttribute("statusCode", status);
        return "myorder/close";
    }

    /**
     * Rendelés lezárás POST metódus.
     * @param order A rendelés.
     * @param status A kivánt státusz.
     * @param model Spring áltat nyújtott objektum.
     * @param redirectAttributes Átírányítás objektum.
     * @return Siker esetén főoldalra átirányítás.
     */
    @PostMapping("/close")
    public String closeOrder(Order order, @RequestParam Integer status, Model model,
                             RedirectAttributes redirectAttributes) {
        order.setStatus(OrderStatus.fromInteger(status).toString());
        var serviceResult =  orderService.updateOrderStatus(order);
        if (serviceResult != OrderService.Result.OK) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "ORDER_CLOSE_ERROR")));
            return closeOrder(order.getSupplierId(), status, model);
        }

        redirectAttributes.addFlashAttribute(
                new Messages().success(Localized.message(messageSource, "ORDER_CLOSE_SUCCESS")));
        return "redirect:/my/orders";
    }

    /**
     * Rendelés tétel hozzáadás GET megjelenítés.
     * @param orderItem A rendelés tétel objektum.
     * @param orderId A rendelés azonosítója.
     * @param model Spring áltat nyújtott objektum.
     * @return myorder/item/add thymeleaf sablon.
     */
    @GetMapping("/item/add")
    public String addItem(OrderItem orderItem, @RequestParam Integer orderId, Model model) {
        model.addAttribute("action", "/my/orders/item/add");
        var order = orderService.getOrder(orderId);
        var orderForm = OrderForm.fromOrder(
                        order,
                        supplierService.getSupplierById(order.getSupplierId()));
        orderItem.setOrderId(orderId);
        model.addAttribute(orderItem);
        model.addAttribute(orderForm);
        return "myorder/item/add";
    }

    /**
     * Rendelés tétel hozzáadás POST metódus.
     * @param orderItem A rendelés tétel objektum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring áltat nyújtott objektum.
     * @return Siker esetén a kiválasztott rendelés aloldalra visszavezetés.
     */
    @PostMapping("/item/add")
    public String addItem(@Validated OrderItem orderItem, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return addItem(orderItem, orderItem.getOrderId(), model);
        }

        var serviceResult = orderService.addItem(orderItem);
        if (serviceResult != OrderService.Result.OK) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "ORDER_ITEM_INSERT_ERROR")));
            return addItem(orderItem, orderItem.getOrderId(), model);
        }

        return "redirect:/my/orders/edit?id="+orderItem.getOrderId();
    }

    /**
     * Rendelés tétel módosítás GET megjelenítés.
     * @param orderId A rendelés azonosítója.
     * @param lineNumber A tétel azonosítója.
     * @param model Spring áltat nyújtott objektum.
     * @return myorder/item/item thymeleaf sablon.
     */
    @GetMapping("/item/edit")
    public String editItem(@RequestParam Integer orderId, @RequestParam Integer lineNumber,
                            Model model) {
        model.addAttribute("action", "/my/orders/item/edit");
        var orderItem = orderService.getOrderItem(orderId, lineNumber);
        var order = orderService.getOrder(orderId);
        var orderForm = OrderForm.fromOrder(
                order,
                supplierService.getSupplierById(order.getSupplierId()));
        model.addAttribute(orderItem);
        model.addAttribute(orderForm);
        return "myorder/item/edit";
    }

    /**
     * Rendelés tétel módosítás POST metódus.
     * @param orderItem A rendelés tétel objektum.
     * @param result Spring által létrehozott BindingResult objektum.
     * @param model Spring áltat nyújtott objektum.
     * @return Siker esetén a kiválasztott rendelés aloldalra visszavezetés.
     */
    @PostMapping("/item/edit")
    public String editItem(@Valid OrderItem orderItem,
                           BindingResult result, Model model) {
        if (result.hasErrors()) {
            var order = orderService.getOrder(orderItem.getOrderId());
            var orderForm = OrderForm.fromOrder(
                    order,
                    supplierService.getSupplierById(order.getSupplierId()));
            model.addAttribute(orderItem);
            model.addAttribute(orderForm);
            return "myorder/item/edit";
        }

        var serviceResult = orderService.editItem(orderItem);
        if (serviceResult != OrderService.Result.OK) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "ORDER_ITEM_UPDATE_ERROR")));
            return editItem(orderItem.getOrderId(), orderItem.getLineNumber(), model);
        }
        return "redirect:/my/orders/edit?id="+orderItem.getOrderId();
    }

    /**
     * Rendelés tétel törlés GET megjelenítés.
     * @param orderId A rendelés azonosítója.
     * @param lineNumber A tétel azonosítója.
     * @param model Spring áltat nyújtott objektum.
     * @return myorder/item/delete thymeleaf sablon.
     */
    @GetMapping("/item/delete")
    public String deleteItem(@RequestParam Integer orderId, @RequestParam Integer lineNumber, Model model) {
        model.addAttribute("action", "/my/orders/item/delete");
        var orderItem = orderService.getOrderItem(orderId, lineNumber);
        model.addAttribute(orderItem);
        return "myorder/item/delete";
    }

    /**
     * Rendelés tétel törlés POST metódus.
     * @param orderItem A rendelés tétel objektum.
     * @param model Spring áltat nyújtott objektum.
     * @return Siker esetén a kiválasztott rendelés aloldalra visszavezetés.
     */
    @PostMapping("/item/delete")
    public String deleteItem(OrderItem orderItem, Model model) {
        model.addAttribute("action", "/my/orders/item/delete");
        var serviceResult = orderService.deleteItem(orderItem);
        if (serviceResult != OrderService.Result.OK) {
            model.addAttribute(
                    new Messages().error(Localized.message(messageSource, "ORDER_ITEM_DELETE_ERROR")));
            return deleteItem(orderItem.getOrderId(), orderItem.getLineNumber(), model);
        }
        return "redirect:/my/orders/edit?id="+orderItem.getOrderId();
    }

}
