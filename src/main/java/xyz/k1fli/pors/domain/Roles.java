package xyz.k1fli.pors.domain;

/**
 * Szerepkör enumeráció.
 */
public enum Roles {
    ROLE_ADMIN,
    ROLE_USER
}
