package xyz.k1fli.pors.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import xyz.k1fli.pors.component.validation.RepeatedField;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Collection;


/**
 * A Felhasználó Domain osztály.
 */
@RepeatedField(
        field = "password",
        message = "{password.mismatch}",
        groups = {User.Create.class, User.ChangePassword.class}
        )
@ToString
@EqualsAndHashCode
public class User implements UserDetails {

    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private String role;

    @Getter
    @Setter
    @Valid
    private Profile profile;

    @Setter
    private boolean enabled;

    @Getter
    @Setter
    @NotBlank(groups = {Create.class, Update.class}, message = "{username.not.blank}")
    @Email(groups = {Create.class, Update.class}, message = "{email.invalid}")
    private String username;

    @Getter
    @Setter
    @Length(groups = {Create.class, ChangePassword.class} ,min = 8, max = 12, message = "{password.range}")
    private String password;

    @Getter
    @Setter
    private transient String repeatedPassword;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (role != null) {
            var authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority(role));
            return authorities;
        }
        return null;
    }


    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public interface Create {}

    public interface Update {}

    public interface ChangePassword {}

}
