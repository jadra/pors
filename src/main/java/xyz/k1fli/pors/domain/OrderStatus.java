package xyz.k1fli.pors.domain;

/**
 * Rendelés státusz enumeráció.
 */
public enum OrderStatus {
    NEW,
    CLOSED_COMPLETE,
    CLOSED_CANCELED;

    public static OrderStatus fromInteger(Integer status) {
        OrderStatus orderStatus;

        if (status == 0) {
            orderStatus = OrderStatus.NEW;
        } else if (status == 1) {
            orderStatus = OrderStatus.CLOSED_COMPLETE;
        } else if (status == 2) {
            orderStatus = OrderStatus.CLOSED_CANCELED;
        } else {
            orderStatus = OrderStatus.NEW;
        }
        return orderStatus;
    }
}