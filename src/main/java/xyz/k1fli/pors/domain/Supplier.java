package xyz.k1fli.pors.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A Beszállító Domain osztály.
 */
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class Supplier implements Serializable {

    private Integer id;

    @NotBlank(message = "{name.not.blank}")
    @Size(min = 2, max = 255, message = "{name.size.range}")
    private String name;

    @Size(max = 255, message = "{contact.name.size.max}")
    private String contactName;

    @Email(message = "{email.invalid}")
    private String contactEmail;

    @Size(max = 50, message = "{contact.phone.size.max}")
    private String contactPhone;

    @Size(max = 500, message = "{address.size.max}")
    private String address;

    @Size(max = 255, message = "{city.size.max}")
    private String city;

    @Size(max = 10, message = "{postalcode.size.max}")
    private String postalCode;
}
