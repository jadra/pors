package xyz.k1fli.pors.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;


/**
 * A Profil Domain osztály.
 */
@ToString
@EqualsAndHashCode
public class Profile {

    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private String role;

    @Getter
    @Setter
    @Min(value = 1, message = "{department.selection.required}")
    private Integer departmentId;

    @Getter
    @Setter
    private Department department;

    @Getter
    @Setter
    private Integer userId;

    @Getter
    @Setter
    @NotNull(groups = {PartialUpdate.class}, message = "{lastname.not.null}")
    @Length(groups = {Default.class, PartialUpdate.class}, min = 2, max = 255, message = "{lastname.size.range}")
    private String lastName;

    @Getter
    @Setter
    @NotNull(groups = {PartialUpdate.class}, message = "{firstname.not.null}")
    @Length(groups = {Default.class, PartialUpdate.class}, min = 2, max = 255, message = "{firstname.size.range}")
    private String firstName;

    @Getter
    @Setter
    @Length(groups = {Default.class, PartialUpdate.class}, max = 50, message = "{telephone1.size.max}")
    private String phone1;

    @Getter
    @Setter
    @Length(groups = {Default.class, PartialUpdate.class}, max = 50, message = "{telephone2.size.max}")
    private String phone2;

    public interface PartialUpdate {}
}
