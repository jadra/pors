package xyz.k1fli.pors.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


/**
 * A Pager osztály a lapozáshoz szükséges információkat
 * tartalmazza.
 */
@RequiredArgsConstructor
public class Pager {
    @Getter
    @Setter
    private int totalRows;

    @Getter
    @Setter
    private int pages;

    @Getter
    @Setter
    private int rowsPerPage;

    private int currentPage;

    @Getter
    private int offset;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        if (currentPage < 0) {
            this.currentPage = 0;
        }
        this.currentPage = currentPage;
        setOffset();
    }

    public void setOffset() {
        var offset = (currentPage == 0 ? 0 :  currentPage - 1) * rowsPerPage;
        this.offset = offset > totalRows ? 0 : offset;
    }

    public int getPagesDisplay() {
        return pages == 0 ? 1 : pages;
    }
}
