package xyz.k1fli.pors.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * A T paraméter osztálynak az adatbázisbol kinyert
 * listát és aktuális pager objetkum tárolója.
 * @param <T> Domain osztály.
 */
@Getter
@AllArgsConstructor
public class PagedResult<T> {

    private final List<T> results;
    private final Pager pager;
}
