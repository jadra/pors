package xyz.k1fli.pors.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Részleg Domain osztály.
 */
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class Department implements Serializable {

    private Integer id;

    @NotBlank(message = "{name.not.blank}")
    @Size(min = 2, max = 255, message = "{name.size.range}")
    private String name;
}
