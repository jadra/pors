package xyz.k1fli.pors.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Rendelés tétel Domain osztály.
 */
@Getter
@Setter
public class OrderItem implements Serializable {
    private Integer orderId;
    private Integer lineNumber;
    @NotBlank(message = "{name.not.blank}")
    private String name;
    @Length(max = 255, message = "{description.size.max}")
    private String description;
    @NotNull(message = "{quantity.not.blank}")
    @NumberFormat(style = NumberFormat.Style.DEFAULT)
    @Min(value = 1, message = "{quantity.size.min}")
    private Integer quantity;
    @NotNull(message = "{unitprice.not.blank}")
    @NumberFormat(style = NumberFormat.Style.DEFAULT)
    @Min(value = 1, message = "{unitprice.size.min}")
    private BigDecimal unitPrice;

    public BigDecimal getTotal() {
        return unitPrice.multiply(new BigDecimal(quantity));
    }
}
