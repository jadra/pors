package xyz.k1fli.pors.domain;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;


/**
 * Rendelés Domain osztály.
 */
@Getter
@Setter
public class Order implements Serializable {
    private Integer id;
    private Integer userId;
    @Min(value = 1, message = "{supplier.selection.required}")
    private Integer supplierId;
    private Supplier supplier;
    private String status;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime deletedAt;
    private List<OrderItem> items;
    private BigDecimal total;

    public BigDecimal calculateTotal() {
        return items.stream()
                .map(OrderItem::getTotal)
                .reduce(new BigDecimal(0), BigDecimal::add);
    }

}
