package xyz.k1fli.pors.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import xyz.k1fli.pors.PorsApplication;
import xyz.k1fli.pors.domain.*;
import xyz.k1fli.pors.mapper.OrderMapper;
import xyz.k1fli.pors.mapper.ProfileMapper;
import xyz.k1fli.pors.mapper.SupplierMapper;
import xyz.k1fli.pors.mapper.UserMapper;


import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PorsApplication.class)
@Transactional
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class UserServiceTest {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProfileMapper profileMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    @Autowired
    private  JdbcUserDetailsManager jdbcUserDetailsManager;

    @Autowired
    private  PasswordEncoder passwordEncoder;

    UserService userService;

    @BeforeEach
    void beforeEach() {
        userService = new UserService();
        userService.setUserMapper(userMapper);
        userService.setJdbcUserDetailsManager(jdbcUserDetailsManager);
        userService.setPasswordEncoder(passwordEncoder);
        userService.setProfileMapper(profileMapper);
    }


    @Test
    void createUser() {
        var profile = new Profile();
        profile.setLastName("Big");
        profile.setFirstName("Jack");
        profile.setDepartmentId(1);
        var user = new User();
        user.setUsername("jack@mail.com");
        user.setPassword("password");
        user.setEnabled(true);
        user.setRole(Roles.ROLE_USER.toString());
        user.setProfile(profile);
        var result = userService.createUser(user);
        assertEquals(UserService.Result.OK, result);
    }

    @Test
    @WithMockUser(username = "admin@site.com", password = "p@ssword!", roles = "ADMIN")
    void updateUser() {
        var profile = new Profile();
        profile.setLastName("Big");
        profile.setFirstName("Jack");
        profile.setDepartmentId(1);
        var user = new User();
        user.setUsername("jack@mail.com");
        user.setPassword("password");
        user.setEnabled(true);
        user.setRole(Roles.ROLE_USER.toString());
        user.setProfile(profile);
        userService.createUser(user);
        var userToUpdate = userService.getByName("jack@mail.com");
        var userProfile = user.getProfile();
        userProfile.setLastName("Biggo");
        userToUpdate.setUsername("jack.biggo@mymail.com");
        userToUpdate.setRole(Roles.ROLE_ADMIN.toString());
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        var result = userService.updateUser(userToUpdate, authentication);
        assertEquals(UserService.Result.OK, result);
    }

    @Test
    void updateDuplicateUserNameFails() {
        var profile = new Profile();
        profile.setLastName("Big");
        profile.setFirstName("Jack");
        profile.setDepartmentId(1);
        var user = new User();
        user.setUsername("jack@mail.com");
        user.setPassword("password");
        user.setEnabled(true);
        user.setRole(Roles.ROLE_USER.toString());
        user.setProfile(profile);
        userService.createUser(user);
        var userToUpdate = userService.getByName("jack@mail.com");
        userToUpdate.setUsername("admin@site.com");
        var result = userService.updateUser(userToUpdate, null);
        assertEquals(UserService.Result.DUPLICATE_NAME, result);
    }

    @Test
    void deleteUser() {
        var profile = new Profile();
        profile.setLastName("Big");
        profile.setFirstName("Jack");
        profile.setDepartmentId(1);
        var user = new User();
        user.setUsername("jack@mail.com");
        user.setPassword("password");
        user.setEnabled(true);
        user.setRole(Roles.ROLE_USER.toString());
        user.setProfile(profile);
        userService.createUser(user);
        var userToDelete = userService.getByName("jack@mail.com");
        var result =  userService.deleteUser(userToDelete);
        assertEquals(UserService.Result.OK, result);
    }

    @Test
    void deleteUserWithOrderFails() {
        var profile = new Profile();
        profile.setLastName("Big");
        profile.setFirstName("Jack");
        profile.setDepartmentId(1);
        var user = new User();
        user.setUsername("jack@mail.com");
        user.setPassword("password");
        user.setEnabled(true);
        user.setRole(Roles.ROLE_USER.toString());
        user.setProfile(profile);
        userService.createUser(user);
        var theUser = userService.getByName("jack@mail.com");
        var supplier = new Supplier();
        supplier.setName("XYZ Company");
        supplierMapper.insert(supplier);
        var theSupplier = supplierMapper.getByName("XYZ Company");
        var order = new Order();
        order.setSupplierId(theSupplier.getId());
        order.setUserId(theUser.getId());
        orderMapper.insert(order);
        var result = userService.deleteUser(theUser);
        assertEquals(UserService.Result.FOREIGN_KEY_VIOLATION, result);
    }

    @Test
    void passwordChange() {
        var profile = new Profile();
        profile.setLastName("Big");
        profile.setFirstName("Jack");
        profile.setDepartmentId(1);
        var user = new User();
        user.setUsername("jack@mail.com");
        user.setPassword("password");
        user.setEnabled(true);
        user.setRole(Roles.ROLE_USER.toString());
        user.setProfile(profile);
        userService.createUser(user);
        var theUser = userService.getByName("jack@mail.com");
        var result = userService.changePassword("jack@mail.com", "password!");
        assertEquals(UserService.Result.OK, result);
    }

}