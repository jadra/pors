package xyz.k1fli.pors.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.k1fli.pors.domain.Department;
import xyz.k1fli.pors.mapper.DepartmentMapper;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class DepartmentServiceTest {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Test
    void createDepartment() {
        var deptService = new DepartmentService(departmentMapper);
        var department = new Department();
        department.setName("Finance");
        deptService.createDepartment(department);
        assertEquals(2, departmentMapper.getAll().size());
    }

    @Test
    void getDepartments() {
        var deptService = new DepartmentService(departmentMapper);
        var department = new Department();
        department.setName("Finance");
        deptService.createDepartment(department);
        var departments = deptService.getDepartments();
        assertTrue(departments.size() > 0);
    }

    @Test
    void updateDepartment() {
        var deptService = new DepartmentService(departmentMapper);
        var department = new Department();
        department.setName("finance");
        deptService.createDepartment(department);
        var departmentToUpdate = deptService.
                getDepartments().
                stream().
                filter(d -> d.getName().equals("finance")).
                findFirst();
        assertTrue(departmentToUpdate.isPresent());
        departmentToUpdate.get().setName("Finance");
        var result = deptService.updateDepartment(departmentToUpdate.get());
        assertEquals(DepartmentService.Result.OK, result);
    }

    @Test
    void deleteDepartment() {
        var deptService = new DepartmentService(departmentMapper);
        var department = new Department();
        department.setName("finance");
        deptService.createDepartment(department);
        var departmentToDelete = deptService.
                getDepartments().
                stream().
                filter(d -> d.getName().equals("finance")).
                findFirst();
        assertTrue(departmentToDelete.isPresent());
        var result = deptService.deleteDepartment(departmentToDelete.get());
        assertEquals(DepartmentService.Result.OK, result);
    }

    @Test
    void getDepartmentById() {
        var deptService = new DepartmentService(departmentMapper);
        var department = deptService.getDepartmentById(1);
        assertNotNull(department);
    }

}