package xyz.k1fli.pors.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.k1fli.pors.domain.Pager;
import xyz.k1fli.pors.domain.Supplier;
import xyz.k1fli.pors.mapper.SupplierMapper;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class SupplierServiceTest {

    @Autowired
    private SupplierMapper supplierMapper;

    @Test
    void createSupplier() {
        var supplierService = new SupplierService(supplierMapper);
        var supplier = new Supplier();
        supplier.setName("XYZ Company");
        var result = supplierService.createSupplier(supplier);
        assertEquals(SupplierService.Result.OK, result);
    }

    @Test
    void getAllPaged() {
        var supplierService = new SupplierService(supplierMapper);
        IntStream.range(0, 10).forEachOrdered(n -> {
            var supplier = new Supplier();
            supplier.setName("company " + n);
            supplierMapper.insert(supplier);
        });
        var pager = new Pager();
        pager.setRowsPerPage(10);
        var pagedResult = supplierService.getAll("company", pager);
        assertEquals(10, pagedResult.getResults().size());
        assertEquals(1, pagedResult.getPager().getPages());
    }

    @Test
    void getAll() {
        var supplierService = new SupplierService(supplierMapper);
        IntStream.range(0, 10).forEachOrdered(n -> {
            var supplier = new Supplier();
            supplier.setName("company " + n);
            supplierMapper.insert(supplier);
        });
        var suppliers =  supplierService.getAll();
        assertEquals(10, suppliers.size());
    }

    @Test
    void getSupplierById() {
        var supplierService = new SupplierService(supplierMapper);
        var supplier = new Supplier();
        supplier.setName("Company 1");
        supplierService.createSupplier(supplier);
        var supplierId = supplierMapper.getByName("Company 1").getId();
        var supplierById = supplierService.getSupplierById(supplierId);
        assertNotNull(supplierById);
        assertEquals("Company 1", supplierById.getName());
    }

    @Test
    void updateSupplier() {
        var supplierService = new SupplierService(supplierMapper);
        var supplier = new Supplier();
        supplier.setName("Company 1");
        supplierService.createSupplier(supplier);
        var theSupplier = supplierMapper.getByName("Company 1");
        theSupplier.setName("Company XYZ");
        var result = supplierService.updateSupplier(theSupplier);
        assertEquals(SupplierService.Result.OK, result);
    }

    @Test
    void updateSupplierFails() {
        var supplierService = new SupplierService(supplierMapper);
        IntStream.range(0, 2).forEachOrdered(n -> {
            var supplier = new Supplier();
            supplier.setName("company " + n);
            supplierMapper.insert(supplier);
        });
        var supplier = supplierMapper.getByName("company 1");
        supplier.setName("company 0");
        var result = supplierService.updateSupplier(supplier);
        assertEquals(SupplierService.Result.DUPLICATE_NAME, result);
    }

    @Test
    void deleteSupplier() {
        var supplierService = new SupplierService(supplierMapper);
        var supplier = new Supplier();
        supplier.setName("Company 1");
        supplierService.createSupplier(supplier);
        var theSupplier = supplierMapper.getByName("Company 1");
        var result = supplierService.deleteSupplier(theSupplier);
        assertEquals(SupplierService.Result.OK, result);
    }
}