package xyz.k1fli.pors.mapper;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import xyz.k1fli.pors.controller.form.OrderFilterForm;
import xyz.k1fli.pors.domain.*;

import java.math.BigDecimal;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@MybatisTest
@Transactional
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class OrderMapperTest {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    private void createOrder(Integer supplierId) {
        var order = new Order();
        order.setSupplierId(supplierId);
        order.setUserId(1);
        orderMapper.insert(order);
    }

    private Supplier createSupplier(String name) {
        var supplier = new Supplier();
        supplier.setName(name);
        supplierMapper.insert(supplier);
        return supplierMapper.getByName(name);
    }


    @Test
    void getPagingInfo() {
        createOrder(createSupplier("company").getId());
        var filter = new OrderFilterForm();
        var pagingInfo = orderMapper.getPagingInfo(1, filter, 10);
        assertEquals(1, pagingInfo.getPages());
        assertEquals(1, pagingInfo.getTotalRows());
    }

    @Test
    void getOrder() {
        createOrder(createSupplier("company").getId());
        var pager = new Pager();
        pager.setRowsPerPage(10);
        var orders = orderMapper.getOrders(1, new OrderFilterForm(),pager);
        var order = orders.get(0);
        var theOrder = orderMapper.getOrder(order.getId());
        assertEquals(theOrder.getId(), order.getId());
    }

    @Test
    void getOrders() {
        var supplierId = createSupplier("company").getId();
        IntStream.range(0, 5).forEachOrdered(n -> {
            createOrder(supplierId);
        });
        var pager = new Pager();
        pager.setRowsPerPage(10);
        var orders = orderMapper.getOrders(1, new OrderFilterForm(), pager);
        assertEquals(5, orders.size());
    }

    @Test
    void getOrderSummary() {
        createOrder(createSupplier("company").getId());
        var pager = new Pager();
        pager.setRowsPerPage(10);
        var orders = orderMapper.getOrders(1, new OrderFilterForm(),pager);
        var order = orders.get(0);

        var orderItem1 = new OrderItem();
        orderItem1.setOrderId(order.getId());
        orderItem1.setName("Box of pens");
        orderItem1.setDescription("box of 10 pens");
        orderItem1.setQuantity(1);
        orderItem1.setUnitPrice(new BigDecimal(1000));

        var orderItem2 = new OrderItem();
        orderItem2.setOrderId(order.getId());
        orderItem2.setName("Projector");
        orderItem2.setDescription("Projector for meeting room");
        orderItem2.setQuantity(1);
        orderItem2.setUnitPrice(new BigDecimal(125500));

        orderMapper.addOrderItem(orderItem1);
        orderMapper.addOrderItem(orderItem2);

        var summary = orderMapper.getOrderSummary(order.getId(), 1);
        assertEquals(new BigDecimal("126500.0000"), summary.getTotal());

    }

    @Test
    void getOrderItem() {
        createOrder(createSupplier("company").getId());
        var pager = new Pager();
        pager.setRowsPerPage(10);
        var orders = orderMapper.getOrders(1, new OrderFilterForm(),pager);
        var order = orders.get(0);

        var orderItem1 = new OrderItem();
        orderItem1.setOrderId(order.getId());
        orderItem1.setName("Machine gear");
        orderItem1.setDescription("Gear for production machine");
        orderItem1.setQuantity(1);
        orderItem1.setUnitPrice(new BigDecimal(50000));

        var orderItem2 = new OrderItem();
        orderItem2.setOrderId(order.getId());
        orderItem2.setName("Lubrication oil");
        orderItem2.setDescription("Oil for machine gear");
        orderItem2.setQuantity(1);
        orderItem2.setUnitPrice(new BigDecimal(7600));

        orderMapper.addOrderItem(orderItem1);
        orderMapper.addOrderItem(orderItem2);

        var oilOrderItem =  orderMapper.getOrderItem(order.getId(), 2);
        assertEquals("Lubrication oil" ,oilOrderItem.getName());
        assertEquals(new BigDecimal("7600.0000"), oilOrderItem.getUnitPrice());
    }

    @Test
    void updateOrderStatus() {
        createOrder(createSupplier("company").getId());
        var pager = new Pager();
        pager.setRowsPerPage(10);
        var orders = orderMapper.getOrders(1, new OrderFilterForm(),pager);
        var order = orders.get(0);
        order.setStatus(OrderStatus.CLOSED_CANCELED.toString());
        orderMapper.updateOrderStatus(order);
        var updatedOrder = orderMapper.getOrder(order.getId());
        assertEquals(OrderStatus.CLOSED_CANCELED.toString(), updatedOrder.getStatus());
    }

    @Test
    void update() {
        createOrder(createSupplier("company 1").getId());
        var newSupplier = createSupplier("company 2");
        var pager = new Pager();
        pager.setRowsPerPage(10);
        var orders = orderMapper.getOrders(1, new OrderFilterForm(),pager);
        var order = orders.get(0);
        order.setSupplierId(newSupplier.getId());
        orderMapper.update(order);
        assertEquals(newSupplier.getId(), orderMapper.getOrder(order.getId()).getSupplierId());
    }

    @Test
    void editOrderItem() {
        createOrder(createSupplier("company").getId());
        var pager = new Pager();
        pager.setRowsPerPage(10);
        var orders = orderMapper.getOrders(1, new OrderFilterForm(),pager);
        var order = orders.get(0);

        var orderItem1 = new OrderItem();
        orderItem1.setOrderId(order.getId());
        orderItem1.setName("Machine gear");
        orderItem1.setDescription("Gear for production machine");
        orderItem1.setQuantity(1);
        orderItem1.setUnitPrice(new BigDecimal(50000));

        orderMapper.addOrderItem(orderItem1);
        var theOrderItem = orderMapper.getOrderItem(order.getId(), 1);
        theOrderItem.setUnitPrice(new BigDecimal(45000));
        orderMapper.editOrderItem(theOrderItem);

        var updatedOrderItem = orderMapper.getOrderItem(order.getId(), 1);
        assertEquals(new BigDecimal("45000.0000"), updatedOrderItem.getUnitPrice());
    }

    @Test
    void deleteOrderItem() {
        createOrder(createSupplier("company").getId());
        var pager = new Pager();
        pager.setRowsPerPage(10);
        var orders = orderMapper.getOrders(1, new OrderFilterForm(),pager);
        var order = orders.get(0);

        var orderItem1 = new OrderItem();
        orderItem1.setOrderId(order.getId());
        orderItem1.setName("Machine gear");
        orderItem1.setDescription("Gear for production machine");
        orderItem1.setQuantity(1);
        orderItem1.setUnitPrice(new BigDecimal(50000));

        var orderItem2 = new OrderItem();
        orderItem2.setOrderId(order.getId());
        orderItem2.setName("Lubrication oil");
        orderItem2.setDescription("Oil for machine gear");
        orderItem2.setQuantity(1);
        orderItem2.setUnitPrice(new BigDecimal(7600));

        orderMapper.addOrderItem(orderItem1);
        orderMapper.addOrderItem(orderItem2);

        var itemToDelete = orderMapper.getOrderItem(order.getId(), 1);
        orderMapper.deleteOrderItem(itemToDelete);

        var updatedOrder = orderMapper.getOrder(order.getId());
        var itemCount = updatedOrder.getItems().size();
        assertEquals(1, itemCount);
    }
}