package xyz.k1fli.pors.mapper;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import xyz.k1fli.pors.domain.Department;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@MybatisTest
@Transactional
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class DepartmentMapperTest {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Test
    void getDepartmentsCountOne() {
         var departments = departmentMapper.getAll();
        assertEquals(1,(long) departments.size());
    }

    @Test
    void insertDepartment() {
        var count = departmentMapper.getAll().size();
        var expectedCount = count + 1;
        var department = new Department();
        department.setName("Finance");
        departmentMapper.insert(department);
        assertEquals(expectedCount, departmentMapper.getAll().size());
    }

    @Test
    void getByName() {
        var department = new Department();
        department.setName("finance");
        departmentMapper.insert(department);
        var financeDept = departmentMapper.getByName("finance");
        assertEquals(department, financeDept);
    }

    @Test
    void getById() {
        var department = new Department();
        department.setName("finance");
        departmentMapper.insert(department);
        var financeDept = departmentMapper.getById(department.getId());
        assertEquals(department, financeDept);
    }

    @Test
    void update() {
        var department = new Department();
        department.setName("finance");
        departmentMapper.insert(department);
        department.setName("Finance");
        departmentMapper.update(department);
        var financeDept = departmentMapper.getByName("Finance");
        assertEquals("Finance", financeDept.getName() );
    }

    @Test
    void delete() {
        var department = new Department();
        department.setName("finance");
        departmentMapper.insert(department);
        departmentMapper.delete(department);
        var count = departmentMapper.getAll().size();
        assertEquals(1, count);
    }
}