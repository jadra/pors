package xyz.k1fli.pors.mapper;


import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import xyz.k1fli.pors.domain.Supplier;
import java.util.stream.IntStream;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@MybatisTest
@Transactional
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class SupplierMapperTest {

    @Autowired
    private SupplierMapper supplierMapper;

    private void createNumberNameSuppliers(int records) {
        IntStream.range(0, records).forEachOrdered(n -> {
            var supplier = new Supplier();
            supplier.setName(String.valueOf(n));
            supplierMapper.insert(supplier);
        });
    }

    @Test
    void pagingInfoNoRecords() {
       var pager = supplierMapper.getPagingInfo(null, 10);
       assertEquals(0, pager.getPages());
       assertEquals(0,pager.getTotalRows());
    }

    @Test
    void pagingInfoRowsFitPage() {
        createNumberNameSuppliers(4);
        var pager = supplierMapper.getPagingInfo(null, 10);
        assertEquals( 1, pager.getPages());
    }

    @Test
    void pagingInfoRowsMultiplePages() {
        createNumberNameSuppliers(22);
        var pager = supplierMapper.getPagingInfo(null, 10);
        assertEquals(3, pager.getPages());
    }

    @Test
    void filteredQueryPagingInfoAccurate() {
        createNumberNameSuppliers(22);
        var pager = supplierMapper.getPagingInfo("1", 10);
        assertEquals(11, pager.getTotalRows());
        assertEquals(2, pager.getPages());
    }

    @Test
    void filteredQueryHasTwoPages() {
        createNumberNameSuppliers(22);
        var pager = supplierMapper.getPagingInfo("1", 10);
        var suppliers = supplierMapper.getAll("1", pager);
        assertEquals(10, suppliers.size());
        assertEquals(2,pager.getPages());
        suppliers.clear();
        pager.setCurrentPage(2);
        pager.setOffset();
        suppliers.addAll(supplierMapper.getAll("1", pager));
        assertEquals(1, suppliers.size());
    }

    @Test
    void unfilteredQueryPaging() {
        createNumberNameSuppliers(22);
        var pager = supplierMapper.getPagingInfo(null, 10);
        var suppliers = supplierMapper.getAll(null, pager);
        assertEquals(10, suppliers.size());
        assertEquals(3,pager.getPages());
        suppliers.clear();
        pager.setCurrentPage(2);
        pager.setOffset();
        suppliers.addAll(supplierMapper.getAll(null, pager));
        assertEquals(10, suppliers.size());
        suppliers.clear();
        pager.setCurrentPage(3);
        pager.setOffset();
        suppliers.addAll(supplierMapper.getAll(null, pager));
        assertEquals(2, suppliers.size());
    }
}